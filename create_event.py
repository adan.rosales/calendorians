from datetime import datetime, timedelta
from cal_setup import get_calendar_service


def create_appoiment(
                    user_name:str,
                    email:str,
                    hour:int,
                    day:int,
                    month:int,
                    year=None,
                    calendar_id="primary",
                    ):
    # creates one hour event tomorrow 10 AM IST
    service = get_calendar_service()
    d = datetime.now().date()
    if not year:
        year = d.year
    start = datetime(year, month, day, hour)
    start = start.isoformat()
    end = (start + timedelta(hours=.5)).isoformat()

    event_result = service.events().insert(calendarId=calendar_id,
        body={ 
            "summary": f'Call Appoiment created by {user_name}  ', 
            "description": f'This call was received on {d.strftime()}  {email}',
            "start": {"dateTime": start, "timeZone": 'America/CST'}, 
            "end": {"dateTime": end, "timeZone": 'America/CST'},
        }
    ).execute()

