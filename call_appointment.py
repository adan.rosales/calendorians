import os
from create_event import create_appoiment
from speech_detect import SpechService
from speaker import SpeakService


def conversation_example():
    speach_service = SpechService()
    speaker_service = SpeakService()
    speaker_service.said_hellow()
    response1 = speach_service.get_response()
    if 'create' in response1.split():
        message = "Ok! you want to creat an appointment!, what's your name?."
        speaker_service.dynamic_message(message)
        resp = False
        while(not resp):
            posible_name = speach_service.get_response()
            message = f'{posible_name} is correct?.'
            speaker_service.dynamic_message(message)
            response = speach_service.get_response()
            resp = bool(set(response.split()).intersection({'yes', 'correct'}))

        speaker_service.ask_for_phone()
        resp = False
        while(not resp):
            phone_number = speach_service.get_response()
            message = f'{phone_number} is correct?.'
            speaker_service.dynamic_message(message, 'confirm_phone')
            response = speach_service.get_response()
            resp = bool(set(response.split()).intersection({'yes', 'correct'}))

        message = f'''
                    Good {posible_name},  when do you want to schedule?,
                    plese say the month and then the day number,
                    for example May 22.
                '''
        speaker_service.dynamic_message(message, 'ask_date')
        month, day = None, None
        while(not month or not day):
            posible_date = speaker_service.getresponse()
            for word in posible_date.split():
                if word.isdigit() and int(word)>0 and int(word)<=31:
                    day = word
                elif word.lower() in ['january','february','march','april', 'may', 'june', 'july', 'august','september', 'october', 'november', 'december']:
                    month = word
            if not month or not day:
                message = ''' Sorry I don't detect that as a valid date,
                can you repeat please saying first the month and then the day?
                    '''
                speaker_service(message,'repeat_date')

        message = "Perfect, what time works for you on {} {} between 10 am and 5pm Central Time.".format(month ,day)
        speaker_service.dynamic_message(message)
        time = speaker_service.getresponse()
        message = 'Ok, a request for  at  was created,do you want to recibe an email confirmation?'
        speaker_service.dynamic_message(message)
        response = speach_service.getresponse()
        message = 'what is your email adress?'
        speaker_service.dynamic_message(message)
        email_adress = speaker_service.getresponse()
        message = f'perfect, {posible_name}! your appointment was created on {month} {day} , you will recibe a email confirmation on your email adress'
        message += ' , thanks fo call to Conversica and have a great day!'
        speaker_service.dynamic_message(message)
        create_appoiment(
                        posible_name,
                        email_adress,
                        time,
                        day,
                        month)

if __name__ == '__main__':
    conversation_example()