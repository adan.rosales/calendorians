from gtts import gTTS
import os


class SpeakService:
    def __init__(self):
        self.language = 'en'
        self.slow = False

    def dynamic_message(self, message, name='dinamic'):
        '''
            reproduce a message based on a dynamic text
            and save as dinamic by default or gived named
        '''
        myobj = gTTS(text=message, lang=self.language, slow=self.slow)
        myobj.save(f"{name}.mp3")
        os.system(f"mpg321 {name}.mp3 > /dev/null")

    def static_message(self, message):
        '''Just play a specifc premade audio'''
        os.system(f"mpg321 {message}.mp3 > /dev/null")

    def create_hellow(self):
        hello = '''
            Thank you for call to Conversica Sales Channel!;
            to Create an appointment, say create
            to Change an appointment, say change
            for cancel a call say cancel
        '''
        self.dynamic_message(message=hello, name='hello')

    def said_hellow(self):
        self.static_message(message="welcome")

    def ask_for_phone(self):
        message = 'What number  sould we call?.'
        self.static_message('ask_for_phone')