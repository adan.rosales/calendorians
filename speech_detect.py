import speech_recognition as sr


class SpechService:
    def __init__(self):
        self.r = sr.Recognizer()
        self.mic = sr.Microphone()
        # This needs to be fixed acording to your noice level:
        self.r.energy_threshold = 300

    def get_response(self):
        with self.mic as source:
            try:
                audio = self.r.listen(source)
                response1 = self.r.recognize_google(audio)
                print('I hear: ', response1)
                return response1
            except Exception as e:
                print(e)
                return None
