from cal_setup import get_calendar_service
import googleapiclient
from datetime import datetime


def free_time(time_min, time_max, calendar_id="primary"):
    '''
    documentation:
    https://googleapis.github.io/google-api-python-client/docs/dyn/calendar_v3.freebusy.html#query
    '''
    start = datetime.now().replace(microsecond=0).isoformat() + "-04:00"
    end = datetime.now().replace(hour=23, microsecond=0).isoformat() + "-04:00"
    freebusy_query = {
        "timeMin" : time_min,
        "timeMax" : time_max,
        "items" :[
          {
            "id" : calendar_id
          }
        ]
      }
    service = get_calendar_service()
    availability = service.freebusy().query(body=freebusy_query).execute()
    print(availability)
    return availability

if __name__ == '__main__':
     free_time('2021-05-04T07:20:50.52Z',"2021-05-06T07:20:50.52Z")